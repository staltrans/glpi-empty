#!/bin/sh -e

help() {
    echo "Usage:\n $0 -n <name> -v <version> -d <path/to/directory/plugins>" 1>&2
    exit 1
}

if [ $# -eq 0 ]; then
    help
fi

while getopts ":n:v:d:h:" o; do
    case "${o}" in
        n)
            name=${OPTARG}
            ;;
        v)
            version=${OPTARG}
            ;;
        d)
            dest=${OPTARG}
            ;;
        h)
            help
            ;;
        *)
            help
            ;;
    esac
done

if [ -z "$name" ]; then
    echo "Option -n is required" 1>&2
    help
fi

if [ -z "$version" ]; then
    echo "Option -v is required" 1>&2
    help
fi

if [ -z "$dest" ]; then
    echo "Option -d is required" 1>&2
    help
fi

if [ ! -d "$dest" ]; then
    echo "'$dest' is not directory" 1>&2
fi

cname=$(echo $name | tr -dc '[[:alpha:]]')
lname=$(echo $cname | tr '[:upper:]' '[:lower:]')
uname=$(echo $cname | tr '[:lower:]' '[:upper:]')
year=$(date +%Y)

echo $cname $lname $uname $year

plugindir=$dest/$lname

if [ -d "$plugindir" ]; then
    echo "The directory '$plugindir' already exists!" 1>&2
    exit 1
fi

mkdir "$plugindir"

if [ ! -d "$plugindir" ]; then
    echo "Сan't create a directory '$plugindir'" 1>&2
    exit 1
fi

echo "Copy all files:"
cp -r -v "$PWD/." "$plugindir"

echo "Remove files or directories:"
for e in ".git" "mkplugin" "plugin.sh" "dist" "README.md"; do
    rm -rvf "$plugindir/$e"
done

cd "$plugindir"

echo "Move templates:"
for f in `ls *.tpl`; do
   mv -v $f ${f%.*}
done

#drop specific file config from RoboFile
sed -e "/^.*protected \$csfiles.*$/d" -i RoboFile.php

# move xml file
mv -v "plugin.xml" "$lname.xml"

#do replacements
sed \
    -e "s/{NAME}/$cname/" \
    -e "s/{LNAME}/$lname/" \
    -e "s/{UNAME}/$uname/" \
    -e "s/{VERSION}/$version/" \
    -e "s/{YEAR}/$year/" \
    -i setup.php hook.php $lname.xml tools/HEADER README.md .travis.yml
